function CompassAssistant(target) {
	/* this is the creator function for your scene assistant object. It will be passed all the
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
  if(target)
  {
    this.target=target;
  }
  Mojo.Log.info(target);
  Mojo.Log.info(this.target);
  Mojo.Log.info(this.target.id);
  this.T=0;
}

CompassAssistant.prototype.setup = function() {
	/* this function is for setup tasks that have to happen when the scene is Compass created */

	/* use Mojo.View.render to render view templates and add them to the scene, if needed */

	/* setup widgets here */

	/* update the app info using values from our app */

  this.controller.setupWidget("mynote",
          this.attributes = {
              multiline: true,
              autoResizeMax: true,
              enterSubmits: false,
              focus: true,
              changeOnKeyPress: false,
              autoReplace:false
              //textCase: Mojo.Widget.steModeLowerCase
          },
          this.model = {
              value: "",
              disabled: false
     });
  this.controller.setupWidget("noteButton",
  this.attributes = {
  },
  this.buttonModel={
    disabled: false,
    label:"Note"});
	/* add event handlers to listen to events from widgets */
  this.listKeyHandler_bind=this.listKeyHandler.bindAsEventListener(this);
  this.draw=this.drawArrow.bind(this);
  this.lastdirection=0;
  this.noteButtonPress_bind=this.noteButtonPress.bindAsEventListener(this);
};

CompassAssistant.prototype.activate = function(event) {
  this.controller.stageController.setWindowProperties(
                  {   blockScreenTimeout: true});
  Mojo.Event.listen(window,'keypress', this.listKeyHandler_bind ,true);
  Mojo.Event.listen(this.controller.get("noteButton"),Mojo.Event.tap, this.noteButtonPress_bind,true);

  //this.canvas=this.controller.get("compass").getContext('2d');
  //this.drawScale();
  //this.drawArrow(0);
  this.controller.get('id').update('id:'+this.target.id);
  for(var i=0;i<TARGETS.length;i++)
  {
    var t=TARGETS[i];
    t.calcDistance(LOCATION);
    t.calcDirection(LOCATION,DIRECTION);
    //Mojo.Log.info("t.id",t.id,"<>","target.id",this.target.id);
    if(t.id==this.target.id)
    {
      Mojo.Log.info("Got target!",i);
      this.T=t;
    }
  }
  if(this.T.mynote)
    this.buttonModel.buttonClass="affirmative";
  else
    this.buttonModel.buttonClass="";
  this.controller.modelChanged(this.buttonModel);

  this.controller.get('distance').update(this.T.distance+" m");
  this.controller.get('compass').src=this.T.image;
  this.controller.get('text').update(this.T.text);
  this.controller.get('header').update('Note from '+this.T.name);
  this.refresh(this);
};

CompassAssistant.prototype.noteButtonPress = function(event)
{
	Mojo.Log.info("addNote()");
	this.controller=Mojo.Controller.stageController.activeScene();
	this.controller.showDialog({
		template: "note/note-scene",
		assistant: new NoteDialogAssistant(this,{target:this.T,scene:this})
	});
  if(this.T.mynote)
    this.buttonModel.buttonClass="affirmative";
  else
    this.buttonModel.buttonClass="";
  this.controller.modelChanged(this.buttonModel);

  //this.controller.pushScene("note",{id:this.target.id});
};


CompassAssistant.prototype.listKeyHandler = function (event)
{
  //Mojo.Log.info("key=",event.keyCode);
  var key=event.keyCode;
  if(key== 97)
    SHIFT.lon-=.002;
  if(key==100)
    SHIFT.lon+=.002;
  if(key==119)
    SHIFT.lat+=.002;
  if(key==115)
    SHIFT.lat-=.002;
//[20130622-10:09:14.246330] info: key= 119
//[20130622-10:09:15.771627] info: key= 100
//[20130622-10:09:16.042841] info: key= 115

};


CompassAssistant.prototype.drawScale = function()
{

  this.canvas.save();
  this.canvas.translate(128,128);
  this.canvas.rotate(0);
  this.canvas.lineWidth=1;
  this.canvas.strokeStyle='rgb(128,128,128)';
  for(var i=0;i<33;i++)
  {
    this.canvas.beginPath();
    this.canvas.moveTo(0,120);
    this.canvas.lineTo(0,128);
    this.canvas.stroke();
    this.canvas.rotate(Math.PI/16);
  }
  this.canvas.restore();
  this.canvas.save();
  this.canvas.translate(128,128);
  this.canvas.rotate(0);
  this.canvas.lineWidth=3;
  this.canvas.strokeStyle='rgb(128,128,128)';
  for(var i=0;i<17;i++)
  {
    this.canvas.beginPath();
    this.canvas.moveTo(0,116);
    this.canvas.lineTo(0,128);
    this.canvas.stroke();
    this.canvas.rotate(Math.PI/8);
  }
  this.canvas.restore();
  this.canvas.save();
  this.canvas.translate(128,128);
  this.canvas.rotate(0);
  this.canvas.lineWidth=5;
  this.canvas.strokeStyle='rgb(128,32,32)';
  for(var i=0;i<5;i++)
  {
    this.canvas.beginPath();
    this.canvas.moveTo(0,112);
    this.canvas.lineTo(0,128);
    this.canvas.stroke();
    this.canvas.rotate(-1.57);
  }
  this.canvas.restore();
}

CompassAssistant.prototype.drawArrow = function (angle,color)
{
  //this.canvas.height=this.canvas.height;
  //this.canvas.shadowColor="rgb(0,0,0)";
  //this.canvas.shadowOffsetX=8;
  //this.canvas.shadowOffsetY=8;
  //this.canvas.shadowBlur=32;
  this.canvas.save();
  this.canvas.translate(128,128);
  this.canvas.rotate(angle);
  this.canvas.lineWidth=3;
  this.canvas.strokeStyle=color;
  this.canvas.beginPath();
  this.canvas.moveTo(0,-96);
  this.canvas.lineTo(24,96);
  this.canvas.lineTo(0,64);
  this.canvas.lineTo(-24,96);
  this.canvas.lineTo(0,-96);
  this.canvas.stroke();
  this.canvas.lineWidth=1;
  this.canvas.beginPath();
  this.canvas.arc(0,0,5,0, 2 * Math.PI, false);
  this.canvas.fill();
  this.canvas.stroke();
  this.canvas.restore();
  Mojo.Log.info("Draw complete!");
  this.lastdirection=angle;

}

CompassAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
  Mojo.Event.stopListening(window,'keypress', this.listKeyHandler_bind);
  Mojo.Event.stopListening(this.controller.get("noteButton"),Mojo.Event.tap, this.noteButtonPress_bind);
  this.controller.stageController.setWindowProperties(
                  {   blockScreenTimeout: false});
};

CompassAssistant.prototype.refresh = function(A)
{
  if(Mojo.Controller.stageController.activeScene().sceneName!="compass")
    return;
  var controller=Mojo.Controller.stageController.activeScene();
  var target=A.target;
  //var dr=this.draw;
  var lastdirection=A.lastdirection;
    controller.serviceRequest('palm://com.palm.location', {
      method: 'startTracking',
      parameters: {"subscribe":true},
      onSuccess : function (e){
        Mojo.Log.info("getCurrentPosition success, results="+JSON.stringify(e));
        LOCATION.lon=e.longitude+SHIFT.lon;
        LOCATION.lat=e.latitude+SHIFT.lat;
        var angle=e.heading/180*Math.PI;
        DIRECTION.lat=Math.cos(angle);
        DIRECTION.lon=Math.sin(angle);
        Mojo.Log.info("Searching for id",target.id);
        A.T.calcDistance(LOCATION);
        A.T.calcDirection(LOCATION,DIRECTION);
        if(Mojo.Controller.stageController.activeScene().sceneName=="compass")
        {
          Mojo.Log.info("t.direction=",A.T.direction);
          //dr(lastdirection,'rgb(228,228,226)');
          //dr(T.direction,'rgb(32,32,32)');
          controller.get('distance').update(A.T.distance+" m");
          controller.get('compass').src=A.T.image;
          //setTimeout(A.refresh.bind(this,A),3000);
        }
      },
      onFailure : function (e){
        Mojo.Log.info("startTracking failure, results="+JSON.stringify(e));
        DIRECTION.lat=0;
        DIRECTION.lon=0;
        //if(Mojo.Controller.stageController.activeScene().sceneName=="compass")
          //setTimeout(A.refresh.bind(this,A),5000);
        }
    });

}

CompassAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as
	   a result of being popped off the scene stack */
};
