
function ListAssistant() {
  /* this is the creator function for your scene assistant object. It will be passed all the
     additional parameters (after the scene name) that were passed to pushScene. The reference
     to the scene controller (this.controller) has not be established yet, so any initialization
     that needs the scene controller should be done in the setup function below. */
}

ListAssistant.prototype.setup = function() {
  /* this function is for setup tasks that have to happen when the scene is first created */

  /* use Mojo.View.render to render view templates and add them to the scene, if needed */

  /* setup widgets here */

  this.controller.setupWidget("mainlistid",
        this.attributes = {
            itemTemplate: "templates/listitem",
            listTemplate: "templates/listcontainer",
            swipeToDelete: true,
						hasNoWidgets: true,
            reorderable: true,
      fixedHeightItems:true,
      renderLimit: 256,
            emptyTemplate:"templates/emptylist",
         },
         this.model = {
             listTitle: "Закладки",
              items: TARGETS,
          }
    );
  this.controller.setupWidget(Mojo.Menu.appMenu,
    this.attributes = {
        omitDefaultItems: true
    },
    this.model = {
        visible: true,
        items: [
            { label: "Удалить все", command: 'do-ClearBookmarks' },
        ]
    }
    );
  /* add event handlers to listen to events from widgets */
    /*
  if(isTouchpad())
  {
    this.controller.get("back").style.display="block";
    this.controller.setupWidget("back",
      this.attributes = {
      },
      this.model = {
        label : "Назад",
        disabled: false
      }
    );
  this.controller.get("back").style.display="block";
  this.backClick_bind=this.backClick.bindAsEventListener(this);
  }


  this.listDeleteHandler_bind=this.listDeleteHandler.bindAsEventListener(this);
  this.listReorderHandler_bind=this.listReorderHandler.bindAsEventListener(this);
  */
  this.listClickHandler_bind=this.listClickHandler.bindAsEventListener(this);
  this.listKeyHandler_bind=this.listKeyHandler.bindAsEventListener(this);
};

ListAssistant.prototype.activate = function(event) {
  /* put in event handlers here that should only be in effect when this scene is active. For
     example, key handlers that are observing the document */
    Mojo.Event.listen(window,'keypress', this.listKeyHandler_bind ,true);
  //if(Settings.bookmarks.length>0)
  //{
    //this.controller.get('mainlistid').mojo.noticeUpdatedItems(0,TARGETS);
    //Mojo.Log.info(JSON.stringify(TARGETS));
  //}

  Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind,true);
  /*
    Mojo.Event.listen(this.controller.get('mainlistid'),
        Mojo.Event.listDelete,
        this.listDeleteHandler_bind,true);
    Mojo.Event.listen(this.controller.get('mainlistid'),
    Mojo.Event.listReorder,
        this.listReorderHandler_bind,true);
*/
    this.refresh();
  //if(isTouchpad())
    //Mojo.Event.listen(this.controller.get("back"),Mojo.Event.tap, this.backClick_bind,true);

};

ListAssistant.prototype.refresh = function()
{
  if(Mojo.Controller.stageController.activeScene().sceneName!="list")
    return;
  var controller=this.controller;
    this.controller.serviceRequest('palm://com.palm.location', {
      method: 'getCurrentPosition',
      parameters: {},
      onSuccess : function (e){
        Mojo.Log.info("getCurrentPosition success, results="+JSON.stringify(e));
        LOCATION.lon=e.longitude+SHIFT.lon;
        LOCATION.lat=e.latitude+SHIFT.lat;
        var angle=e.heading/180*Math.PI;
        DIRECTION.lat=Math.cos(angle);
        DIRECTION.lon=Math.sin(angle);
        for(var i=0;i<TARGETS.length;i++)
        {
          var t=TARGETS[i];
          t.calcDistance(LOCATION);
          t.calcDirection(LOCATION,DIRECTION);
        }
        if(Mojo.Controller.stageController.activeScene().sceneName=="list")
        controller.get('mainlistid').mojo.noticeUpdatedItems(0,TARGETS);
        if(SETTINGS.autosort)
          TARGETS.sort(function(a,b){return a.distance-b.distance});
        this.controller.window.setTimeout(this.refresh.bind(this),5000);
        },
      onFailure : function (e){
        Mojo.Log.info("startTracking failure, results="+JSON.stringify(e));
        DIRECTION.lat=0;
        DIRECTION.lon=0;
        this.controller.window.setTimeout(this.refresh.bind(this),5000);
        }
    });
}

ListAssistant.prototype.listKeyHandler = function (event)
{
  Mojo.Log.info("key=",event.keyCode);
  var key=event.keyCode;
  if(key== 97)
    SHIFT.lon-=.002;
  if(key==100)
    SHIFT.lon+=.002;
  if(key==119)
    SHIFT.lat+=.002;
  if(key==115)
    SHIFT.lat-=.002;
//[20130622-10:09:14.246330] info: key= 119
//[20130622-10:09:15.771627] info: key= 100
//[20130622-10:09:16.042841] info: key= 115

}

//ListAssistant.prototype.orientationChanged = function(orientation){
//  Mojo.Log.info("Orientation Changed", orientation);
//  this.controller.stageController.setWindowOrientation( orientation );
//}
ListAssistant.prototype.deactivate = function(event) {
  /* remove any event handlers you added in activate and do any other cleanup that should happen before
     this scene is popped or another scene is pushed on top*/
  Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listTap,
        this.listClickHandler_bind);
  /*
    Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listDelete,
        this.listDeleteHandler_bind);
    Mojo.Event.stopListening(this.controller.get('mainlistid'),
        Mojo.Event.listReorder,
        this.listReorderHandler_bind);
  if(isTouchpad())
    Mojo.Event.stopListening(this.controller.get('back'),
        Mojo.Event.tap,
        this.backClick_bind);*/
  Mojo.Event.stopListening(window,'keypress', this.listKeyHandler_bind);
};

ListAssistant.prototype.cleanup = function(event) {
  /* this function should do any cleanup needed before the scene is destroyed as
     a result of being popped off the scene stack */
};

ListAssistant.prototype.listClickHandler = function(event){
  /*var str=""+event.originalEvent.target;*/
  Mojo.Controller.stageController.pushScene("compass", { id:event.item.id});

}
ListAssistant.prototype.listDeleteHandler = function(event){
  //WebSocketTest(event.item.thread,"disconnect");
  //Settings.bookmarks.splice(event.index,1);
  //saveSettings();
}

ListAssistant.prototype.listReorderHandler = function(event){
  //var from=event.fromIndex;
  //var to=event.toIndex;
  //var temp=Settings.bookmarks[from];
  //Settings.bookmarks.splice(from,1);
  //Settings.bookmarks.splice(to,0,temp);
  //saveSettings();
}
ListAssistant.prototype.backClick = function(event) {
  Mojo.Controller.stageController.popScene();
  //closeScene();
};
