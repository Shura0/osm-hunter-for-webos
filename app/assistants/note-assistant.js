var NoteDialogAssistant = Class.create({
  initialize: function(sceneAssistant,param) {
    this.sceneAssistant = sceneAssistant;
    this.controller = sceneAssistant.controller;
    this.target=param.target,
    this.scene=param.scene
  },
  setup : function(widget) {
    this.widget = widget;
    //var lt=Settings.login;
    //var pt=Settings.password;
    this.controller.setupWidget("note",
      this.attributes = {
        hintText: "Add",
        multiline: true,
        enterSubmits: false,
        autoReplace: true,
        autoFocus: true,
        limitResize: true
      },
      this.model = {
        value: this.target.mynote,
        disabled: false
      }
    );
    this.controller.setupWidget("ok",
      this.attributes = {
      },
      this.model = {
        label : "Ok",
        disabled: false
      }
    );
    this.controller.setupWidget("cancel",
      this.attributes = {
      },
      this.model = {
        label : "Cancel",
        disabled: false,
        buttonClass:"negative",
      }
    );
      //this.controller.get('note').mojo.focus();
    this.controller.get('ok').addEventListener(Mojo.Event.tap,
      this.handleButtonok.bind(this));
    this.controller.get('cancel').addEventListener(Mojo.Event.tap,
      this.handleButton.bind(this));
  },
  handleButton: function() {
    this.widget.mojo.close();
  },
  handleButtonok: function() {
    this.target.setNote(this.controller.get('note').mojo.getValue());
    if(this.target.mynote)
    {
      this.target.styleclass='withnote';
      this.scene.buttonModel.buttonClass="affirmative";
      var flag=0;
      for(var i=0;i<SETTINGS.targets.length;i++)
      {
        if(SETTINGS.targets[i].id==this.target.id)
        {
          SETTINGS.targets[i].mynote=this.target.mynote;
          flag=1;
          break;
        }
      }
      if(!flag)
        SETTINGS.targets.push(this.target);
    }
    else
    {
      this.scene.buttonModel.buttonClass="";
      this.target.styleclass="";
      for(var i=0;i<SETTINGS.targets.length;i++)
      {
        if(SETTINGS.targets[i].id==this.target.id)
        {
          SETTINGS.targets[i].mynote=this.target.mynote;
          SETTINGS.targets.splice(i,1);
          break;
        }
      }
    }
    this.scene.controller.modelChanged(this.scene.buttonModel);
    saveSettings();
    this.widget.mojo.close();
  }
});
