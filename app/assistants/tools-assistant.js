function ToolsAssistant() {
	/* this is the creator function for your scene assistant object. It will be passed all the
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
}

//TODO: remove this
var buttonModel={};
var text="";

ToolsAssistant.prototype.setup = function() {

  //saveSettings();
  loadSettings(function(){
    Mojo.Log.info("callback");
    if(TARGETS.length<1)
    {
      this.oldButtonModel.disabled=true;
      this.controller.modelChanged(this.oldButtonModel);
    }
    else
    {
      this.oldButtonModel.disabled=false;
      this.controller.modelChanged(this.oldButtonModel);
    }
    if(SETTINGS.radius)
    {
      this.pickerModel.value=SETTINGS.radius;
      this.controller.modelChanged(this.pickerModel);
    }
  }.bind(this));
  //if(!SETTINGS.radius)
//    SETTINGS.radius=20;
  //saveSettings();
  buttonModel={
    label: "Download new",
    disabled: false
  }
  this.controller.setupWidget("downloadButton",
  this.attributes = {
    type: Mojo.Widget.activityButton
  },
  buttonModel);
  this.controller.setupWidget("oldButton",
  this.attributes = {
  },
  this.oldButtonModel={
    label: "Open old list",
    disabled: true}
    );

  this.controller.setupWidget("radiusPicker",
  this.attributes = {
      label:"Radius (km)",
      modelProperty: 'value',
      min: 5,
      max: 40
  },
  this.pickerModel = {
      value:20
  }
);

this.controller.setupWidget("nameField",
  this.attributes = {
      hintText: $L(" input name"),
      multiline: false,
      enterSubmits: false,
      focus: true
  },
  this.model = {
      value: '',
      disabled: false
  }
);
  this.downloadButtonPress_bind=this.downloadButtonPress.bindAsEventListener(this);
  this.oldButtonPress_bind=this.oldButtonPress.bindAsEventListener(this);
};

ToolsAssistant.prototype.activate = function(event) {
	/* put in event handlers here that should only be in effect when this scene is active. For
	   example, key handlers that are observing the document */
  Mojo.Event.listen(this.controller.get("downloadButton"),Mojo.Event.tap, this.downloadButtonPress_bind);
  Mojo.Event.listen(this.controller.get("oldButton"),Mojo.Event.tap, this.oldButtonPress_bind);
  text='';
  this.controller.get('text').update(text);
  //loadSettings();
  if(TARGETS.length>1)
  {
    this.oldButtonModel.disabled=false;
    this.controller.modelChanged(this.oldButtonModel);
  }
};

ToolsAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
  Mojo.Event.stopListening(this.controller.get("downloadButton"),Mojo.Event.tap, this.downloadButtonPress_bind);
  Mojo.Event.stopListening(this.controller.get("oldButton"),Mojo.Event.tap, this.oldButtonPress_bind);
};

ToolsAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as
	   a result of being popped off the scene stack */
};


ToolsAssistant.prototype.downloadButtonPress = function(event)
{

  buttonModel.disabled=true;
  oldButtonModel=this.oldButtonModel;
  this.oldButtonModel.disabled=true;
  this.controller.modelChanged(buttonModel);
  this.controller.modelChanged(this.oldButtonModel);
  this.controller.get('downloadButton').mojo.activate();
  SETTINGS.radius=this.pickerModel.value;
  text='Getting geolocation...';
  this.controller.get('text').update(text);
  var controller=this.controller;
  this.controller.serviceRequest('palm://com.palm.location', {
    method: 'getCurrentPosition',
    parameters: {},
    onSuccess : function (e){
      text+='ok<br>';
      controller.get('text').update(text);
      Mojo.Log.info("startTracking success, results="+JSON.stringify(e));
      LOCATION.lon=e.longitude;
      LOCATION.lat=e.latitude;
      var angle=e.heading/180*Math.PI;
      DIRECTION.lat=Math.cos(angle);
      DIRECTION.lon=Math.sin(angle);
      var xmlhttp = new XMLHttpRequest();
      Mojo.Log.info("open connection");
      text+='Getting notes list...';
      controller.get('text').update(text)
      var request=makeRequest(SETTINGS.radius);
      xmlhttp.open('GET', request, true);
      Mojo.Log.info("Request="+request);
      xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4) {
          Mojo.Log.info("readystate=4");
          if (xmlhttp.status == 200) {
            Mojo.Log.info("status=200");
            text+='ok<br>';
            controller.get('text').update(text);
            var t=xmlhttp.responseText;
            Mojo.Log.info("Received text="+t);
            notes=JSON.parse(t);
            Mojo.Log.info("Type is:",notes.type);
            for(var j=0;j<SETTINGS.targets.length;j++)
            {
              if(SETTINGS.targets[j] && !SETTINGS.targets[j].mynote)
              {
                SETTINGS.targets.splice(j,1);
                j--;
              }
            }
            for(var i=0;i<notes.features.length;i++)
            {
              Mojo.Log.info("Type is:",notes.features[i].type);
              var n=notes.features[i];
              if(notes.features[i].type=='Feature')
              {
                if(n.geometry && n.geometry.coordinates && n.geometry.type=="Point")
                {

                  var t=new Target(n.geometry.coordinates[1],n.geometry.coordinates[0]);
                  t.id=n.properties.id;
                  t.setText(n.properties.comments[0].text);
                  var name=n.properties.comments[0].user;
                  if(!name)name="Anon";
                  t.calcDistance(LOCATION);
                  t.setName(name);
                  t.calcDirection(LOCATION,DIRECTION);
                  var flag=0;
                  for(var j=0;j<TARGETS.length;j++)
                  {
                    if(TARGETS[j].id == t.id)
                    {
                      flag=1;
                      break;
                    }
                  }
                  if(!flag && (t.distance/1000<SETTINGS.radius))
                    TARGETS.push(t);
                  Mojo.Log.info("Got bug from", name);
                }
              }
            }
            for(var i=0;i<TARGETS.length;i++)
            {
              var len=Distance(LOCATION,TARGETS[i].point);
              Mojo.Log.info("DISTANCE to target "+i+" is "+len+" m");
            }
            TARGETS.sort(function(a,b){return a.distance-b.distance});
            buttonModel.disabled=false;
            oldButtonModel.disabled=false;
            controller.modelChanged(buttonModel);
            controller.modelChanged(oldButtonModel);
            controller.get('downloadButton').mojo.deactivate();
            saveSettings();
            Mojo.Controller.stageController.pushScene("list");
          }
          else {
            switch(xmlhttp.status)
            {
              case 0:
                Mojo.Controller.errorDialog("Невозможно подключиться к сервру");
              break;
              case 403:
                Mojo.Controller.errorDialog("Forbidden<br>Неверный логин или пароль"+xmlhttp.statusText);
              break;
              case 404:
                Mojo.Controller.errorDialog("Not found<br>Something went wrong"+xmlhttp.statusText);
              break;
              default:
                Mojo.Controller.errorDialog(xmlhttp.status+"<br>"+xmlhttp.statusText);
              break;
            }
            buttonModel.disabled=false;
            this.oldButtonModel.disabled=false;
            controller.modelChanged(buttonModel);
            controller.modelChanged(this.oldButtonModel);
            controller.get('downloadButton').mojo.deactivate();
            text+='fail<br>';
            controller.get('text').update(text);
          }
        }
      }
      xmlhttp.send();
    },
    onFailure : function (e){
      text+='fail<br>';
      controller.get('text').update(text);
      Mojo.Log.info("getLocation failure, results="+JSON.stringify(e));
      DIRECTION.lat=0;
      DIRECTION.lon=0;
      buttonModel.disabled=false;
      controller.modelChanged(buttonModel);
      controller.get('downloadButton').mojo.deactivate();
    }
  });
}

ToolsAssistant.prototype.oldButtonPress = function(event)
{
  Mojo.Controller.stageController.pushScene("list");
}

