var TARGETS=[];
var LOCATION=new Point(0,0);
//var LOCATION=new Point(55.8523,37.6473);
var DIRECTION=new Point(0,0);
//lat=55.8523&lon=37.6473
var REQUEST='http://api.openstreetmap.org/api/0.6/notes.json?bbox='+(LOCATION.lon-1)+','+
(LOCATION.lat-1)+','+(LOCATION.lon+1)+','+(LOCATION.lat+1)+'&closed=0';

var SETTINGS={
  targets:[]
  };

var SHIFT=new Point(0,0);

cloneObject=function(source) {
  for (i in source) {
    if (typeof source[i] == 'source') {
        this[i] = new cloneObject(source[i]);
    }
    else{
        this[i] = source[i];
    }
  }
}

function saveSettings() {

  SETTINGS.targets=[];
  var db = new Mojo.Depot({name:'geo'}, function(){
    db.removeAll(function(){
      Mojo.Log.info("settings cleared");
      for(var i=0;i<TARGETS.length;i++)
      {
        db.add("target_"+i, TARGETS[i], function(){Mojo.Log.info("Saved target");}, {})
      }
      db.add("radius", SETTINGS.radius, function(){Mojo.Log.info("Saved radius",SETTINGS.radius)}, {})

    },
    {})},{});
  //var db = new Mojo.Depot({name:'geo'}, function(){

  //},
  //function(){return});
}

function _getNextTarget(db,a,i,f)
{
  if(!a)
  {
    if(f)f();
    return;
  }
  SETTINGS.targets.push(a);
  var t=new Target();
  t.copyFrom(a);
  if(t.mynote)
  {
    t.styleclass="withnote";
  }
  if(!LOCATION.lat && !LOCATION.lon)
  {
    t.distance=0;
    t.direction=0;
    t.image='images/arrows/00.png';
  }
  TARGETS.push(t);
  i++;
  //Mojo.Log.info("getting target id",a.id);
  db.get('target_'+i,function(a){_getNextTarget(db,a,i,f)},function(){if(f)f();Mojo.Log.info("callback from _getNextTarget")});
}

function loadSettings(f) {//callback function
  SETTINGS.targets=[];
  TARGETS=[];
  var i=0;
  var db = new Mojo.Depot(
    {name:'geo'},
    function(){
      db.get('radius',function(a){if(a)SETTINGS.radius=a;else SETTINGS.radius=20;Mojo.Log.info("Loaded radius:",a);},function(){Mojo.Log.info("Cannot load radius");});
      db.get('target_'+i,function(a){_getNextTarget(db,a,i,f)},function(){if(f)f();Mojo.Log.info("callback")});
    },
    function(){}
  );
}


function makeRequest(distance)
{
  var dlat=distance/111; //km per degree latitude
  //var dlon=distance/62; //km per degree longitude
  var lat_rad = LOCATION.lat * Math.PI / 180;
  var dlon=distance/(111*Math.cos(lat_rad));
  return 'http://api.openstreetmap.org/api/0.6/notes.json?bbox='+(LOCATION.lon-dlon)+','+(LOCATION.lat-dlat)+','+(LOCATION.lon+dlon)+','+(LOCATION.lat+dlat)+'&closed=0';
}