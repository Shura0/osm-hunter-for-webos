function StageAssistant() {
	/* this is the creator function for your stage assistant object */
}

StageAssistant.prototype.setup = function() {
	/* this function is for setup tasks that have to happen when the stage is first created */

	/* for a simple application, the stage assistant's only task is to push the scene, making it
	   visible */
	this.controller.pushScene("tools",{id:0});
};

StageAssistant.prototype.handleCommand = function(event) {
	this.controller=Mojo.Controller.stageController.activeScene();
	var type=this.controller.sceneName;
	var assistant=this.controller.assistant;
  if (event.type == Mojo.Event.command) {
    switch (event.command) {
      case "do-ClearAll":
        Mojo.Log.info("Menu pressed",event.command);
        TARGETS=[];
        this.controller.get('mainlistid').mojo.setLength(TARGETS.length);
        //this.controller.get('mainlistid').mojo.noticeUpdatedItems(0,TARGETS);
        saveSettings();
      break;
    }
  }
}
